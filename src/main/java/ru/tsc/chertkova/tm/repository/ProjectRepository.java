package ru.tsc.chertkova.tm.repository;

import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project remove(final Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(String id) {
        for (final Project project :
                projects) {
            if (project.getId().equals(id)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project findByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}
