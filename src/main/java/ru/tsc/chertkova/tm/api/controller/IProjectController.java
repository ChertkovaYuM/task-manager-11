package ru.tsc.chertkova.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProject();

    void createProject();

    void removeProjectByIndex();

    void removeProjectById();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

}
