package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
